--Select content of joined content of Pattern table (just star and and location):

SELECT * FROM pattern, period, route WHERE pattern.route_id = route.id 
AND pattern.period_id = period.id 
AND (route.number = 1 OR route.number = (SELECT MAX(route.number) FROM route GROUP BY id));



--Select content of joined content of Pattern table (with all touched location)

SELECT * FROM pattern, period, route WHERE pattern.route_id = route.id 
AND pattern.period_id = period.id;



--Select routes with ellapsed time each locaiton:

SELECT id as _id, number AS _number, location,  minute, (SELECT SUM(minute) FROM route WHERE id=_id AND number<=_number GROUP BY id) AS ellapsed_time 
FROM route;

--Select reverse routes with ellapsed time each locaiton:

SELECT id as _id, number AS _number, location,  minute, (IFNULL((SELECT SUM(minute) FROM route WHERE id=_id AND number>_number GROUP BY id),0)) AS ellapsed_time 
FROM route;



--Select all location in trip with ellapsed time:

SELECT *, route.number AS _number, route.id AS _id, 
	(SELECT SUM(minute) FROM route WHERE id=_id AND number<=_number GROUP BY id) AS ellapsed_time 
FROM pattern, period, route WHERE pattern.route_id = route.id 
AND pattern.period_id = period.id;





--Select all location in trip with ellapsed time in time format:

SELECT *, route.number AS _number, route.id AS _id, 
	(SELECT SUM(minute) FROM route WHERE id=_id AND number<=_number GROUP BY id) AS ellapsed_time,
    (SELECT MAKETIME(ellapsed_time DIV 60, ellapsed_time MOD 60, 0)) AS start_time
FROM pattern, period, route WHERE pattern.route_id = route.id 
AND pattern.period_id = period.id;




--Select all location in trip with touched time in time format:

SELECT *, route.number AS _number, route.id AS _id, pattern.time AS _time, 
	(SELECT SUM(minute) FROM route WHERE id=_id AND number<=_number GROUP BY id) AS ellapsed_time,
    (SELECT ADDTIME(_time , MAKETIME(ellapsed_time DIV 60, ellapsed_time MOD 60, 0))) AS start_time
FROM pattern, period, route WHERE pattern.route_id = route.id 
AND pattern.period_id = period.id;









--Select the routes with ellapsed time (forward and reverse too), in time:
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SELECT *, route.number AS _number, route.id AS _id, pattern.time AS _time,
	(IF(is_reversed,
        (IFNULL((SELECT SUM(minute) FROM route WHERE id=_id AND number>_number GROUP BY id),0)),
        (SELECT SUM(minute) FROM route WHERE id=_id AND number<=_number GROUP BY id))) AS ellapsed_time,
    (SELECT ADDTIME(_time , MAKETIME(ellapsed_time DIV 60, ellapsed_time MOD 60, 0))) AS start_time
FROM pattern, period, route WHERE pattern.route_id = route.id 
AND pattern.period_id = period.id
ORDER BY pattern.id, route.number;





--Schedule with all of the datas:
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SELECT *, route.number AS _number, route.id AS _id, pattern.time AS _time,
	(IF(is_reversed,
        (IFNULL((SELECT SUM(minute) FROM route WHERE id=_id AND number>_number GROUP BY id),0)),
        (SELECT SUM(minute) FROM route WHERE id=_id AND number<=_number GROUP BY id))) AS ellapsed_time,
    (SELECT ADDTIME(_time , MAKETIME(ellapsed_time DIV 60, ellapsed_time MOD 60, 0))) AS start_time
FROM schedule, bus, pattern, period, route 
WHERE pattern.route_id = route.id 
AND pattern.period_id = period.id
AND schedule.pattern_id = pattern.id
AND schedule.bus_id = bus.id
ORDER BY pattern.id, route.number;



--Trigger to insert into Ticket:
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
DROP TRIGGER before_insert_ticket;

DELIMITER $$
CREATE TRIGGER before_insert_ticket
     BEFORE INSERT ON ticket FOR EACH ROW
     BEGIN
     /*Seat not exists*/
          IF (SELECT count(*) FROM bus, seats WHERE bus.id = (SELECT schedule.bus_id FROM schedule WHERE schedule.id = NEW.schedule_id) AND bus.id = seats.bus_id AND seats.seat= NEW.seat) <> 1
          OR
     /*It is booked*/
          	(SELECT count(*) FROM ticket WHERE schedule_id = NEW.schedule_id AND seat = NEW.seat AND deleted = 0 AND NOT( NEW.to_id <= from_id OR NEW.from_id >= to_id)) <> 0
          THEN
     /*Throw an error*/
			SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'Seat booked or doesn\'t exists';
          END IF;
		  
		IF
	 /*From value wrong*/
			NEW.from_id NOT IN (SELECT route.number FROM schedule,pattern,route WHERE schedule.pattern_id = pattern.id AND pattern.route_id = route.id AND schedule.id = 1)
		  OR
	 /*To value wrong*/
			NEW.to_id NOT IN (SELECT route.number FROM schedule,pattern,route WHERE schedule.pattern_id = pattern.id AND pattern.route_id = route.id AND schedule.id = 1)
          THEN
     /*Throw an error*/
			SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'Location doesn\'t exists in this route';
          END IF;
     END;
$$